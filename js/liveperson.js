

        var urlParams;
        (window.onpopstate = function () {
            var match,
                pl     = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
                query  = window.location.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
               urlParams[decode(match[1])] = decode(match[2]);
        })();
 		window.lpTag=window.lpTag||{};if(typeof window.lpTag._tagCount==='undefined'){window.lpTag={site:localStorage.getItem("account")||accountId,section:lpTag.section||'',autoStart:lpTag.autoStart===false?false:true,ovr:lpTag.ovr||{},_v:'1.6.0',_tagCount:1,protocol:'https:',events:{bind:function(app,ev,fn){lpTag.defer(function(){lpTag.events.bind(app,ev,fn);},0);},trigger:function(app,ev,json){lpTag.defer(function(){lpTag.events.trigger(app,ev,json);},1);}},defer:function(fn,fnType){if(fnType==0){this._defB=this._defB||[];this._defB.push(fn);}else if(fnType==1){this._defT=this._defT||[];this._defT.push(fn);}else{this._defL=this._defL||[];this._defL.push(fn);}},load:function(src,chr,id){var t=this;setTimeout(function(){t._load(src,chr,id);},0);},_load:function(src,chr,id){var url=src;if(!src){url=this.protocol+'//'+((this.ovr&&this.ovr.domain)?this.ovr.domain:'lptag.liveperson.net')+'/tag/tag.js?site='+this.site;}var s=document.createElement('script');s.setAttribute('charset',chr?chr:'UTF-8');if(id){s.setAttribute('id',id);}s.setAttribute('src',url);document.getElementsByTagName('head').item(0).appendChild(s);},init:function(){this._timing=this._timing||{};this._timing.start=(new Date()).getTime();var that=this;if(window.attachEvent){window.attachEvent('onload',function(){that._domReady('domReady');});}else{window.addEventListener('DOMContentLoaded',function(){that._domReady('contReady');},false);window.addEventListener('load',function(){that._domReady('domReady');},false);}if(typeof(window._lptStop)=='undefined'){this.load();}},start:function(){this.autoStart=true;},_domReady:function(n){if(!this.isDom){this.isDom=true;this.events.trigger('LPT','DOM_READY',{t:n});}this._timing[n]=(new Date()).getTime();},vars:lpTag.vars||[],dbs:lpTag.dbs||[],ctn:lpTag.ctn||[],sdes:lpTag.sdes||[],ev:lpTag.ev||[]};lpTag.init();}else{window.lpTag._tagCount+=1;}
    
        window.AssistSDK = {onScreenshareRequest : function() {return true;}};
        var cobrowseServer =sessionStorage.getItem("cobrowseServer");
        if (cobrowseServer) {
            document.addEventListener("DOMContentLoaded", function(e){
                var assistJs = cobrowseServer + "/assistserver/sdk/web/consumer/assist.js";
                var script = document.createElement("script");
                script.id = "assist-cobrowse-script";
                script.type = "text/javascript";
                script.src = assistJs;
                document.body.appendChild(script);
            });
        }
        lpTag.events.bind({
            eventName: "cobrowseAccepted",
            appName: "*",
            func: function(event) {
                console.log(event);
                var elements = document.querySelectorAll('[data-assist-a' + event.agentId + ']');
                
                var data = JSON.parse(atob(elements[elements.length-1].dataset['assistA' + event.agentId]));
                console.log(data);

                var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;
                

                if (iOS == true) {

                 window.location.href = "yourapp://buttonClicked"+"?token="+data.token+"&url="+data.server+"&cid="+data.cid;
                 
                }

                if (window.AssistSDK && AssistSDK.endSupport) {
                    AssistSDK.endSupport();
                }
                var assistJs = data.server + "/assistserver/sdk/web/consumer/assist.js";
                var script = document.createElement("script");
                script.id = "assist-cobrowse-script";
                script.type = "text/javascript";
                script.src = assistJs;
                script.addEventListener("load", function(e) {
                    sessionStorage.setItem("cobrowseServer", data.server);


                    var iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;

                    if (iOS === false) {
                        AssistSDK.startSupport({ url: data.server, sessionToken: data.token, correlationId: data.cid, sdkPath: data.server + "/assistserver/sdk/web/consumer" });
                    }


                });
                document.body.appendChild(script);
            },
            async: true //default is false
        });
        lpTag.events.bind({
            eventName: "*",
            appName: "*",
            func: function(event) {
                if (event.state == "ended" || event.state == "waiting"|| event.state == "chatting") {
                    console.log(event);
                    console.log("Chat starting, ended, or transferred. Cleaning up any co-browse in progress.")
                    console.log(event);
                    if (window.AssistSDK && window.AssistSDK.endSupport) {
                        AssistSDK.endSupport();
                    }
                    var oldScript = document.getElementById("assist-cobrowse-script");
                    if (oldScript) {
                        oldScript.parentNode.removeChild(oldScript);
                    }
                    sessionStorage.removeItem("cobrowseServer");
                }
            },
            async: true //default is false
        });